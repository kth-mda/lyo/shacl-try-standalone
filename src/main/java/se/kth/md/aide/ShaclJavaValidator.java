package se.kth.md.aide;

import es.weso.rdf.PrefixMap;
import es.weso.rdf.jena.RDFAsJenaModel;
import es.weso.schema.Result;
import es.weso.schema.Schema;
import es.weso.schema.Schemas;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.Option;
import scala.util.Try;

/**
 * ShaclJavaValidator is .
 * @author Andrii Berezovskyi (andriib@kth.se)
 * @version $Id$
 * @since 0.0.0
 */
public class ShaclJavaValidator {
  public static final String FILE_DATA = "/good1-d.ttl";
  public static final String FILE_SCHEMA = "/good1-s.ttl";
  public static final String FORMAT_TURTLE = "TURTLE";
  public static final String UTF_8 = "UTF-8";
  public static final String SCHEMA_SHACLEX = "SHACLex";
  public static final Option<String> OPTION_NONE = Option.apply(null);
  private final static Logger log = LoggerFactory.getLogger(ShaclJavaValidator.class);

  public Result invoke() throws IOException {
    CharSequence rdfCharSequence = load(FILE_DATA, "Error reading data from stream");
    CharSequence streamAsCharSequence = load(FILE_SCHEMA, "Error reading schema from stream");

    Try<RDFAsJenaModel> rdfTry = RDFAsJenaModel.fromChars(rdfCharSequence, FORMAT_TURTLE,
      OPTION_NONE);

    return validate_v55(OPTION_NONE, rdfTry, streamAsCharSequence);
  }

  private CharSequence load(final String resourcePath, final String msg) throws IOException {
    InputStream schemaStream = App.class.getResourceAsStream(resourcePath);
    CharSequence streamAsCharSequence;
    try {
      streamAsCharSequence = streamAsCharSequence(schemaStream);
    } catch (IOException e) {
      log.error(msg, e);
      throw e;
    }
    return streamAsCharSequence;
  }

  private CharSequence streamAsCharSequence(final InputStream inputStream) throws IOException {
    return IOUtils.toString(inputStream, Charset.forName(UTF_8));
  }

  private Result validate_v55(final Option<String> none, final Try<RDFAsJenaModel> rdf_try,
    final CharSequence streamAsCharSequence) {
    Try<Schema> schema_try = Schemas.fromString(streamAsCharSequence, FORMAT_TURTLE, SCHEMA_SHACLEX,
      none);

    if (rdf_try.isSuccess() && schema_try.isSuccess()) {
      RDFAsJenaModel rdf = rdf_try.get();
      Schema schema = schema_try.get();

      PrefixMap nodeMap = rdf.getPrefixMap();
      PrefixMap shapesMap = schema.pm();

      Result r = schema.validate(rdf, "TargetDecls", none, none, nodeMap, shapesMap);

      log.info(r.show());
      return r;
    }
    return null;
  }

  private void validate_v54(final CharSequence rdfstreamAsCharSequence,
    final CharSequence streamAsCharSequence) {
//    Option<String> base = Option.apply(null);
//    List<String> emptyErrors = new ArrayList<>();
//    Try<Schema> t = Schemas.fromString(streamAsCharSequence, "TURTLE", "SHACLex", base);
//    if (t.isSuccess()) {
//      Schema s = t.get();
//      Try<String> tstr = s.serialize("TURTLE");
//      if (tstr.isSuccess()) {
//        Try<RDFAsJenaModel> trdf = RDFAsJenaModel.fromChars(rdfstreamAsCharSequence, "TURTLE",
//          base);
//        if (trdf.isSuccess()) {
//          Result r = s.validate(trdf.get());
//          String strResult = r.serialize("TURTLE");
//          log.debug(strResult);
//        }
//      }
//    }
  }
}
