package se.kth.md.aide;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hello world!
 */
public class App {
  
  private final static Logger log = LoggerFactory.getLogger(App.class);
  
  public static void main(String[] args) {
    System.out.println("Hello World!");

    tryValidate();
  }

  private static void tryValidate() {
    ShaclJavaValidator validator = new ShaclJavaValidator();
    try {
      validator.invoke();
    } catch (IOException e) {
      log.error("Error invoking validator", e);
    }
  }
}
