package se.kth.md.aide;

import es.weso.schema.Result;
import es.weso.schema.Solution;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import scala.collection.Seq;

public class ShaclJavaValidatorTest {
  @Test
  public void testShaclexRunsWithoutExceptions() throws Exception {
    ShaclJavaValidator validator = new ShaclJavaValidator();

    validator.invoke();
  }

  @Test
  public void testShaclexReturnsResult() throws Exception {
    ShaclJavaValidator validator = new ShaclJavaValidator();

    Result result = validator.invoke();

    assertThat(result).isNotNull();
  }

  @Test
  public void testShaclexResultHasMessage() throws Exception {
    ShaclJavaValidator validator = new ShaclJavaValidator();

    Result result = validator.invoke();

    assertThat(result.message()).isNotBlank();
  }

  @Test
  public void testShaclexResultsSolutionsIterable() throws Exception {
    ShaclJavaValidator validator = new ShaclJavaValidator();
    Result result = validator.invoke();

    Seq<Solution> solutionSeq = result.solutions();

    assertThat(solutionSeq.size()).isGreaterThan(0);
  }
}
