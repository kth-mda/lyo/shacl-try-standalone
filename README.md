# Standalone SHACLEX-Java test app

> WARNING! This code is obsolete since the release of [Lyo Validation](https://github.com/eclipse/lyo/tree/master/validation).

See the `ShaclJavaValidatorTest` unit test as a starting point.

Visit the [maven site](http://assume.gitlab.io/shacl-try-standalone/dependencies.html) for the dependency report.

## License

Licensed under the EUPL-1.2-or-later.
